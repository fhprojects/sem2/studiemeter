export interface RatingItem {
    title: string; 
    value: number;
}