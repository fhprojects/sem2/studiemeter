import React from "react";
import {useNavigate} from "react-router-dom";
import {Button, Card, Form, Input, Layout, PageHeader, Space, Typography} from "antd";

export const Debug = () => {
    const navigate = useNavigate();

    return (
        <Layout className="h100">
            <PageHeader
                style={{background: "#ffa814"}}
                className="site-page-header"
                title="Studiemeter"
                subTitle="Debug Dashboard"
                ghost={false}
            />
            <div style={{
                padding: "4em",
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center'
            }}>
                <Card>
                    <Space direction="vertical">
                        <Typography.Paragraph>
                            <Typography.Text >Select a page to continue</Typography.Text>
                        </Typography.Paragraph>

                        <Button type="primary" block style={{background: "#eb9400", borderColor: "#ffaf27"}} onClick={() => navigate("/debug/join")}>Join</Button>
                        <Button type="primary" block style={{background: "#eb9400", borderColor: "#ffaf27"}} onClick={() => navigate("/debug/dashboard/student")}>Student View</Button>
                        <Button type="primary" block style={{background: "#eb9400", borderColor: "#ffaf27"}} onClick={() => navigate("/debug/dashboard/teacher")}>Teacher Dashboard</Button>
                        <Button type="primary" block style={{background: "#eb9400", borderColor: "#ffaf27"}} onClick={() => navigate("/debug/evaluation")}>Evaluation</Button>
                    </Space>
                </Card>
            </div>
        </Layout>
    );
};