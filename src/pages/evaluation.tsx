import {Card, Layout, PageHeader, Tabs, Typography} from "antd";
import {Graph} from "../components/graph";
import {Comments} from "../components/comments";
import {Session} from "../components/session";

export const Evaluation = () => {
    return (
        
        <Layout className="h100">
            <PageHeader
                style={{background: "var(--accent)"}}
                className="site-page-header"
                title="Studiemeter"
                subTitle="Evaluation"
                ghost={false}
                extra={<Session popup/>}
            />
            <div className="card-background">
                <Tabs defaultActiveKey="1" centered>
                    <Tabs.TabPane key="tab1" tab="Rate">
                        <div className="card-background">
                            <div className="evaluation__coefficient">
                                <Typography.Title level={3}>Effectiveness Coefficient</Typography.Title>
                                <span className="evaluation__coefficient-value">
                                    5.7
                                </span>
                            </div>
                            <Graph graphTitle="Teacher-Ratings"/>
                            <Graph graphTitle="Student-Ratings"/>
                        </div>
                    </Tabs.TabPane>
                    <Tabs.TabPane key="tab2" tab="Comments">
                        <div style={{
                            padding: "0em 4em 4em 4em",
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>
                            <Typography.Paragraph>
                                <Typography.Title level={4}>Student Comments</Typography.Title>
                                <Typography.Text type="secondary">Comments are anonymous and can only be viewed by
                                    you</Typography.Text>
                            </Typography.Paragraph>
                            <Card className="card-background">
                                <Comments/>
                            </Card>
                        </div>
                    </Tabs.TabPane>
                </Tabs>
            </div>
        </Layout>
    )
}