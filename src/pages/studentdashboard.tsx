// Header: Graph with median | "Finish Session" button
// Tabs: Give Feedback | View Comments
// Footer: Join session with session code / QR-Code

import {Button, Card, Form, Layout, PageHeader, Row, Space, Typography} from "antd";
import {useState} from "react";
import {Rating} from "../components/rating";
import {Session} from "../components/session";
import TextArea from "antd/lib/input/TextArea";
import { RatingItem } from "../models/rating-item";

const defaultRatingItems: RatingItem[] = [{
        title: 'Friendliness',
        value: 0
    }, {
        title: 'Lecture Quality',
        value: 0
    }, {
        title: 'Distractions',
        value: 0
    }, {
        title: 'Lorem',
        value: 0
    }, {
        title: 'Ipsum',
        value: 0
    }, {
        title: 'Dolor sit amet',
        value: 0
    }
];

export function StudentDashboard() {
    const [loading, setLoading] = useState<boolean>();
    const [comment, setComment] = useState<string>();
    const [ratings, setRatings] = useState<RatingItem[]>(defaultRatingItems);

    const enterLoading = () => {
        setLoading(true)
        setTimeout(() => {
            setLoading(false);
            setComment('');
            setRatings(defaultRatingItems);
        }, 3000);
    };

    const setRating = (name: string, value: number) => {
        const newRatings = [...ratings.map(i => {return {...i}})];
        let item = newRatings.find(i => i.title === name);
        if(item){
            item.value = value;
            setRatings(newRatings);
        }
    }

    const getRatings = () => 
        ratings.map((item, index) => {
            return (
                <Rating title={item.title} 
                        key={index}
                        value={item.value} 
                        onChange={v => setRating(item.title, v)}/>
            )
        });

    return (
        <Layout className="h100">
            <PageHeader
                style={{background: "var(--accent)"}}
                className="site-page-header"
                title="Studiemeter"
                subTitle="Student"
                ghost={false}
                extra={<Session/>}
            />
            <Form className="student-view" onSubmitCapture={enterLoading}>
                <Space direction="vertical">
                    <Typography.Paragraph>
                        <Typography.Title level={4}>Submit rating</Typography.Title>
                        <Typography.Text type="secondary">It is not required to rate all categories at
                            once</Typography.Text>
                    </Typography.Paragraph>
                    <Card className="card-background">
                        <Row wrap gutter={[24, 24]}>
                            {getRatings()}
                        </Row>
                        <TextArea value={comment} 
                                  className="student-view__comment"
                                  onChange={e => setComment(e.target.value)} 
                                  name="comment" 
                                  placeholder="Please write a comment..."
                                  />
                    </Card>
                    <Button
                        style={{float: "right", background: "#eb9400", borderColor: "#ffaf27"}}
                        loading={loading}
                        type="primary"
                        htmlType="submit">
                        Send
                    </Button>
                </Space>
            </Form>
        </Layout>
    );
}
