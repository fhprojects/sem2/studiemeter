// Header: Graph with median | "Finish Session" button
// Tabs: Give Feedback | View Comments
// Footer: Join session with session code / QR-Code

import {Button, Card, Divider, Layout, PageHeader, Row, Space, Tabs, Typography} from "antd";
import React, {useState} from "react";
import {Graph} from "../components/graph";
import {Comments} from "../components/comments";
import {Rating} from "../components/rating";
import {Session} from "../components/session";


export function TeacherDashboard() {
    const [loading, setLoading] = useState<boolean>();

    const enterLoading = () => {
        setLoading(true)
        setTimeout(() => {
            setLoading(false)
        }, 3000);
    };

    return (
        <Layout className="h100">
            <PageHeader
                style={{background: "#ffa814"}}
                className="site-page-header"
                title="Studiemeter"
                subTitle="Teacher"
                ghost={false}
                extra={<Session popup/>}
            />
            <div style={{background: "#fff"}}>
                <div style={{background: "#f5f6f9"}}>
                    <Graph/>
                </div>
                <Tabs defaultActiveKey="1" centered>
                    <Tabs.TabPane key="tab1" tab="Rate">
                        <div style={{
                            padding: "0em 4em 4em 4em",
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>
                            <Space direction="vertical">
                                <Typography.Paragraph>
                                    <Typography.Title level={4}>Submit student rating</Typography.Title>
                                    <Typography.Text type="secondary">It is not required to rate all categories at
                                        once</Typography.Text>
                                </Typography.Paragraph>
                                <Card style={{background: "#fafcff"}}>
                                    <Row wrap gutter={[24, 24]}>
                                        <Rating title="Attention"/>
                                        <Rating title="Contribution"/>
                                        <Rating title="A"/>
                                        <Rating title="B"/>
                                        <Rating title="C"/>
                                        <Rating title="D"/>
                                    </Row>
                                </Card>
                                <Button
                                    style={{float: "right", background: "#eb9400", borderColor: "#ffaf27"}}
                                    loading={loading}
                                    type="primary"
                                    onClick={() => enterLoading()}>
                                    Send
                                </Button>
                            </Space>
                        </div>
                    </Tabs.TabPane>
                    <Tabs.TabPane key="tab2" tab="Comments">
                        <div style={{
                            padding: "0em 4em 4em 4em",
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>
                            <Typography.Paragraph>
                                <Typography.Title level={4}>Student Comments</Typography.Title>
                                <Typography.Text type="secondary">Comments are anonymous and can only be viewed by
                                    you</Typography.Text>
                            </Typography.Paragraph>
                            <Card style={{background: "#fafcff"}}>
                                <Comments/>
                            </Card>
                        </div>
                    </Tabs.TabPane>
                </Tabs>
            </div>
        </Layout>
    );
}
