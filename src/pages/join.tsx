import React, {useState} from "react";
import {Button, Card, Divider, Form, Input, Layout, PageHeader, Space, Spin, Typography} from "antd";

export const Join = () => {
    const [loading, setLoading] = useState<boolean>();

    const id = undefined; // todo: Get id from feedback.domain.at/<ID>

    if (id) {
        // Forward to student dashboard
        return (<>
            <span>Loading session...</span>
            <Spin/>
        </>);
    }

    const onFinish = (values: any) => {
        console.log('Success:', values);
    };


    const enterLoading = () => {
        setLoading(true)
        setTimeout(() => {
            setLoading(false)
        }, 3000);
    };

    return (
        <Layout className="h100">
            <PageHeader
                style={{background: "#ffa814"}}
                className="site-page-header"
                title="Studiemeter"
                subTitle="Student"
                ghost={false}
            />
            <div style={{
                padding: "4em",
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center'
            }}>
                <Card>
                    <Space direction="vertical">
                        <Typography.Paragraph>
                            <Typography.Title level={4}>Join a session</Typography.Title>
                            <Typography.Text type="secondary">Type in a session code to continue</Typography.Text>
                        </Typography.Paragraph>
                        <Form
                            name="basic"
                            labelCol={{span: 4}}
                            onFinish={onFinish}
                            requiredMark={false}
                        >
                            <Form.Item
                                name="sessioncode"
                                rules={[{required: true, message: 'Session code is required!'}]}
                            >
                                <Input placeholder="Session code (81247, ...)"/>
                            </Form.Item>
                            <Form.Item style={{margin: 0}} >
                                <Button
                                    block
                                    htmlType="submit"
                                    style={{background: "#eb9400", borderColor: "#ffaf27"}}
                                    loading={loading}
                                    type="primary"
                                    onClick={() => enterLoading()}>
                                    Join
                                </Button>
                            </Form.Item>
                        </Form>
                    </Space>
                </Card>
            </div>
        </Layout>
    );
};