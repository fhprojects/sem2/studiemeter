import React from "react";
import {IUserInfo} from "../services/userInfo";
import {TeacherDashboard} from "./teacherdashboard";
import {StudentDashboard} from "./studentdashboard";
import {ISessionInfo} from "../services/sessionInfo";
import {useNavigate} from "react-router-dom";

export const Dashboard = () => {
    const navigate = useNavigate();

    const sessionInfo: ISessionInfo | any = undefined; // todo use sessionInfo hook
    const userInfo: IUserInfo | any = undefined; // todo use userInfo hook

    // todo reenable
    // if (!sessionInfo) {
    //     console.error("Invalid session!")
    //     navigate("/join")
    // }

    // todo reenable
    // return userInfo.teacher ? <TeacherDashboard/> : <StudentDashboard/>;

    return <TeacherDashboard/>
};