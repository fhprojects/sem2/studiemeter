import 'antd/dist/antd.css';
import './sass/index.sass'
import React from 'react';
import {BrowserRouter, Navigate, Route, Routes} from "react-router-dom";
import {Join} from "./pages/join";
import {Dashboard} from "./pages/dashboard";
import {StudentDashboard} from "./pages/studentdashboard";
import {TeacherDashboard} from "./pages/teacherdashboard";
import {Debug} from "./pages/debug";
import { Evaluation } from './pages/evaluation';

function App() {
    return (
            <BrowserRouter basename= {`${process.env.PUBLIC_URL}`}>
                <Routes>
                    <Route path="join" element={<Join/>}/>
                    <Route path="dashboard" element={<Dashboard/>}/>

                    <Route path="debug" element={<Debug/>}/>
                    <Route path="debug/join" element={<Join/>}/>
                    <Route path="debug/evaluation" element={<Evaluation/>}/>
                    <Route path="debug/dashboard/teacher" element={<TeacherDashboard/>}/>
                    <Route path="debug/dashboard/student" element={<StudentDashboard/>}/>

                    {/*<Route path="*" element={<Navigate to="/join" replace/>}/>*/}
                    <Route path="*" element={<Navigate to="/debug" replace/>}/>
                </Routes>
            </BrowserRouter>
    );
}

export default App;
