import {CheckboxProps} from "antd/lib/checkbox/Checkbox";
import {Badge, Checkbox, Row} from "antd";
import React from "react";

export interface ICheckBoxColoredProps extends CheckboxProps {
    color: string,
}

export const AxisCheckBox = (props: ICheckBoxColoredProps) => (
    <Row>
        <Badge color={props.color}/>
        <Checkbox {...props} />
    </Row>
);