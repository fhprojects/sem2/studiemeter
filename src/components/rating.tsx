import {BookOutlined} from "@ant-design/icons";
import {Col, Rate, RateProps, Typography} from "antd";
import {useState} from "react";

export interface IRatingProps extends RateProps {
    title: string;
}

export function Rating(props: IRatingProps) {
    const tooltips = ["Bad", "Poor", "Average", "Great", "Excellent"]

    const [value, setValue] = useState();

    const handleChange = (value: any) => {
        setValue(value);
    };

    return (
        <Col sm={24} md={12} lg={8} xl={6}>
            <Typography.Paragraph>
                <Typography.Text strong>{props.title}</Typography.Text>
            </Typography.Paragraph>
            <Rate allowHalf
                  defaultValue={0} {...props}
                  tooltips={tooltips}
                  style={{color: '#d68700'}}
                  character={<BookOutlined style={{fontSize: '40px'}}/>}/>
        </Col>
    );
}