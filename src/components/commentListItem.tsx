import {Avatar, List, Skeleton, Typography} from "antd";
import React from "react";
import {UserOutlined} from "@ant-design/icons";

export interface ICommentProps {
    timestamp: Date | number;
    content: string | JSX.Element;

    rating: {
        rating1: number;
        // rating2 : number;
        // rating3 : number; // todo common interface
    }

    loading: boolean;
}

export const CommentListItem = (props: { item: ICommentProps }) => {
    const dateString = new Date(props.item.timestamp).toLocaleString();

    return (
        <List.Item>
            <Skeleton avatar title={false} loading={props.item.loading} active>
                <List.Item.Meta
                    avatar={<Avatar icon={<UserOutlined/>}/>}
                    title={<Typography.Text type="secondary">{dateString}</Typography.Text>}
                    description= {<Typography.Text >{props.item.content}</Typography.Text>}
                />
            </Skeleton>
        </List.Item>
    )
};
