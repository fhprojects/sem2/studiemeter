import React from "react";
import {CommentListItem, ICommentProps} from "./commentListItem";
import {List} from "antd";
import {Scrollbars} from 'react-custom-scrollbars-2';

const data: ICommentProps[] = [
    {
        content: "Hello",
        timestamp: Date.now(),
        loading: false,
        rating: {rating1: 4}
    }, {
        content: "This lecture is exciting!",
        timestamp: Date.now(),
        loading: false,
        rating: {rating1: 9}
    }, {
        content: "Monotone voice",
        timestamp: Date.now(),
        loading: false,
        rating: {rating1: 5}
    }, {
        content: "I'm hungry",
        timestamp: Date.now(),
        loading: false,
        rating: {rating1: 8}
    }, {
        content: "I already know how to do this",
        timestamp: Date.now(),
        loading: false,
        rating: {rating1: 3}
    }, {
        content: "Difficult topic, but great explanation!",
        timestamp: Date.now(),
        loading: false,
        rating: {rating1: 7}
    }, {
        content: "Hard to grasp",
        timestamp: Date.now(),
        loading: false,
        rating: {rating1: 6}
    },
]

export class Comments extends React.Component<any, any> {

    render() {
        return (
            <Scrollbars style={{minHeight: 440}}>
                <List
                    dataSource={data}
                    renderItem={(item: ICommentProps) => (
                        <CommentListItem item={item}/>
                    )}
                />
            </Scrollbars>
        );
    }
}