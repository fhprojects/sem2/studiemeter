import {useEffect, useState} from "react";
import {Space, Tooltip, Typography} from "antd";
import {Area} from "@ant-design/charts";
import {InfoCircleOutlined} from "@ant-design/icons";


export function Graph({graphTitle = 'Live Rating Graph '}) {
    const [data, setData] = useState([]);
    useEffect(() => {
        setData(require("./graphDataDummy.json"))
    }, []);

    const config = {
        data,
        xField: 'timestamp',
        yField: 'value',
        isStack: false,
        smooth: true,
        xAxis: {
            label: {
                formatter: (timestamp: any) => {
                    const parsed = Date.parse(timestamp);
                    return !isNaN(parsed) ? new Date(parsed).toLocaleTimeString() : timestamp;
                }
            }
        },
        seriesField: 'category',
    };


    return (
        <Space direction="vertical" style={{width: "100%", padding: "1em 2em"}}>

            <Typography.Title level={5}>{graphTitle}<Tooltip
                title="Click on the labels to show or hide graphs">
                <Typography.Text type="secondary">
                    <InfoCircleOutlined style={{fontSize: "10pt"}}/>
                </Typography.Text>
            </Tooltip>
            </Typography.Title>

            <Area style={{maxHeight: "20em"}} {...config} />
        </Space>
    );
}