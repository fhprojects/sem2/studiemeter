import React from "react";
import {Button, Col, Divider, Popover, Row, Space, Typography} from "antd";

// Create session
// Show Session ID / QR-CODE
// create session and create teacher session token


// todo get session code dynamically

import qr from "../res/join-qr.svg"

export interface ISessionProps {
    popup?: boolean;
}

export const Session = (props: ISessionProps) => {
    const popup = (
        <>
            <Space direction="vertical" style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
                <Typography.Title level={4}>Join Session</Typography.Title>
                <img style={{maxWidth: "160px"}} src={qr} alt="Session 16578 QR code"/>
            </Space>
            <Divider style={{margin: "6x 0px"}}/>
            <Button danger block>End Session</Button>
        </>
    );

    const codeDisplay = (
        <Button type="text">
            <Typography.Text strong>Session: </Typography.Text>
            <Typography.Text keyboard className="session">16578</Typography.Text>
        </Button>
    );

    if (props.popup) {
        return (<Popover placement="topRight" content={popup}>
            {codeDisplay}
        </Popover>);
    }

    return codeDisplay;
};