// Provide sessionInfo hook

export interface ISessionInfo {
    id : number;
    created : Date;
    completed : Date | undefined;
}