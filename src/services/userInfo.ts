// Provide userInfo hook

export interface IUserInfo {
    id : string;
    dbToken : string;
    teacher : boolean;
}