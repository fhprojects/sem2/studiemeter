![banner](doc/banner.png)

This project is intended as a clickdummy for the group project "studiemeter", used in SPS2 at [FH-Hagenberg](https://www.fh-ooe.at/en/hagenberg-campus/). It can be found at https://fh.sgruber.at/sem2/sps/studiemeter.

## Deployment

#### `yarn build`

Builds the app for production to the `build` folder.
Read the docs about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

#### NGINX configuration

Config for deployment at https://fh.sgruber.at/sem2/sps/studiemeter.


```
server {
    server_name fh.sgruber.at;
    index index.html index.htm;

    root /var/www/fh;

    location / {
        try_files $uri $uri/ /index.html =404;
    }

    location ~ ^/sem2/sps/studiemeter {
         try_files $uri $uri/ /sem2/sps/studiemeter/index.html =404;
    }

    # listen 80; / listen 443;
    # SSL stuff
}
```

## Development

In the project directory, you can run:

#### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

#### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.


---

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).


